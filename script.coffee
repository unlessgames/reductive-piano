NOTESDOM = ["C", "C-", "D", "D-", "E", "F", "F-", "G", "G-", "A", "A-", "B"]
NOTES = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
NOTESLIB = ["C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"]

instrument = "orchestral_harp-mp3"# "acoustic_bass-mp3" # #"clarinet" #"acoustic_grand_piano"
noteso = (start, octave) ->
  r = []
  console.log octave
  for i in [start..11]
    r.push "#{NOTESLIB[i]}#{octave}"
  r

ALLNOTES = noteso(9, 0).concat(noteso(0,1).concat(noteso(0,2).concat(noteso(0,3).concat(noteso(0,4).concat(noteso(0,5).concat(noteso(0,6).concat(noteso(0,7))))))))

sharpify = (ns) ->
  s = 9
  i = 0
  for n in ns
    if n.length is 3 then ns[i] = NOTES[(s + i) % 12] + n[2]
    i++
  ns

ALLNOTESSHARP = sharpify ALLNOTES.slice(0)
buffers = {}
for n, i in ALLNOTES
  buffers[ALLNOTESSHARP[i]] = new Tone.Buffer("./#{instrument}/#{n}.mp3")

newvoice = () ->
  r = 
  r

#fak

filter = new Tone.Filter(2000, "lowpass").toMaster();

class voice
  note : "X"
  player : {}
  envelope : {}
  play : (n) ->
    @.player.buffer = buffers[n]
    @.note = n
    @.player.start(0)
    @.envelope.triggerAttack()
  stop : (n) ->
    @.note = "X"
    # @.player.stop(0)
    @.envelope.triggerRelease()
  constructor : () ->
    @.player = new Tone.Player()
    @.envelope = new Tone.AmplitudeEnvelope({
                  "attack": 0.02,
                  "decay": 0.2,
                  "sustain": 1.0,
                  "release": 1.0
    }).connect(filter)
    @.player.connect(@.envelope)
    @

voices = (v) ->
  vs = []
  for i in [0..v - 1]
    vs.push new voice()
  vs

polysynth =
  voices : voices(8)
  play : (n) ->
    # console.log n
    playing = false
    for v in @.voices
      if v.note is "X" and not playing
        v.play(n)
        playing = true
  stop : (n) ->
    for v in @.voices
      if v.note is n then v.stop()


newScale = (root, t, o) ->
  ri = 0
  for n, i in NOTESDOM
    if root is n then ri = i
  major = [0, 2, 4, 5, 7, 9, 11]
  chromatic = [0,1,2,3,4,5,6,7,8,9,10,11]
  switch t
    when "major" then major.map((n) -> NOTESDOM[(ri + n) % NOTESDOM.length] + o)
    when "chromatic" then chromatic.map((n) -> NOTESDOM[(ri + n) % NOTESDOM.length] + o)

keylives = 1
keys = newScale("C", "chromatic", 3).concat(newScale("C", "chromatic", 4))
keycodetonote = [81, 50, 87, 51, 69, 82, 53, 84, 54, 89, 55, 85,   90, 83, 88, 68, 67, 86, 71, 66, 72, 78, 74, 77]
keystate = keycodetonote.map((k) -> false)
keys.push "C5"

pianolayout = [1,2,1,2,1,1,2,1,2,1,2,1]

instakey = (i, o) ->
  cls = if pianolayout[i] is 1 then "key-white" else "key-black"
  # 1 3 6 8 10
  css = if i is 1 or i is 6 then {"margin-left" : "14px", "margin-right" : "6px" } else if i is 3 or i is 10 then { "margin-left" : "6px", "margin-right" : "13px" } else  { "margin-left" : "5px", "margin-right" : "5px" }
  # console.log(css)
  ky = $("<div>", { id : "#{keys[i + o * 12]}", "class": cls}) #, html:"#{NOTES[i]}" }  )
  if cls isnt "key-white" then ky.css(css)
  ky.mousedown( (e) -> 
               $(e.target).addClass "playing"
               console.log e.target.id.substring(0, e.target.id.length - 1)
               onnote({note:{name: e.target.id.substring(0,e.target.id.length - 1), octave : -3 + parseInt(e.target.id[e.target.id.length - 1])}})
               )
  ky.on("mouseup mouseleave", (e) -> 

             $(e.target).removeClass "playing"
             offnote({note:{name: e.target.id.substring(0,e.target.id.length - 1), octave : -3 + parseInt(e.target.id[e.target.id.length - 1])}})
             )
  if cls is "key-white" then $("#piano_white").append(ky) else $("#piano_black").append(ky)
  

instapiano = (octaves) ->
  $("#piano_white").html ""
  $("#piano_black").html ""
  wksize = 30
  bksize = 30
  ksize = (t) -> if t is 1 then wksize else bksize
  for o in [0..octaves-1]
    for i in [0..11]
      instakey(i, o)
  instakey(0, octaves)


centerpiano = () -> $("#piano").css {left:window.innerWidth / 2 - 180, top : window.innerHeight / 2 - 50}
window.onresize = centerpiano
lives = []

init = () ->
  lives = keys.map((k) -> keylives)
  centerpiano()
  instapiano(2)

onnote = (e) ->
  console.log e
  n = e.note.name.replace("-", "#") + (e.note.octave + 3)
  idn = n.slice(0).replace("#", '-')
  i = keys.indexOf(idn)
  if i isnt -1
    if lives[i] > 0 then polysynth.play n
    $("#" + idn).animate({opacity:"-=0.1"}, 100)
    $("#" + idn).addClass "playing"
    lives[i]--

offnote = (e) ->
  n = e.note.name.replace("-", "#") + (e.note.octave + 3)
  idn = n.slice(0).replace("#", '-')
  i = keys.indexOf(idn)
  if i isnt -1
    $("#" + idn).animate({opacity:lives[i] / keylives}, 100, () -> $("#" + idn).removeClass "playing" )
  polysynth.stop n

WebMidi.enable( 
  (err) ->
    if err then console.log("WebMidi could not be enabled.", err)

    console.log(WebMidi.inputs)
    if WebMidi.inputs.length > 0
      input = WebMidi.inputs[0]

      input.addListener('noteon', "all", onnote)
      input.addListener('noteoff', "all", offnote)
    # input.addListener('noteon', "all", (n)-> polysynth.play(n.note.name))
    # input.addListener('noteoff', "all", offnote)
    )

reset = () ->
  lives.fill(keylives)
  init()

keyboardoffset = 36

$(document).keydown((e) -> 
                    if e.keyCode is 32 then init()
                    console.log e.keyCode
                    i = keycodetonote.indexOf(e.keyCode)
                    if not keystate[i]
                      if i isnt -1 then onnote({note:{name:NOTESDOM[i % 12], octave:(if i - 12 >= 0 then 1 else 0)}})
                      keystate[i] = true
)
$(document).keyup((e) -> 
                    i = keycodetonote.indexOf(e.keyCode)
                    if keystate[i]
                      if i isnt -1 then offnote({note:{name:NOTESDOM[i % 12], octave:(if i - 12 >= 0 then 1 else 0)}})
                      keystate[i] = false
)

# keyboard.keyUp = (note) ->
init()
