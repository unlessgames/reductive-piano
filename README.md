## reductive piano

a piano with disappearing keys

using 
[Tone.js](https://tonejs.github.io/),
[midi-js-soundfonts](https://github.com/gleitz/midi-js-soundfonts),
[webmidi.js](https://github.com/cotejp/webmidi)